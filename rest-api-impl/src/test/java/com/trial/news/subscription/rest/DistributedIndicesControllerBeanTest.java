package com.trial.news.subscription.rest;

import com.trial.news.subscription.domain.api.SubscriptionIndexService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class DistributedIndicesControllerBeanTest {

    @Mock
    private SubscriptionIndexService service;

    @InjectMocks
    private DistributedIndicesControllerBean controller;

    @Test
    public void triggerIndexRebuildingAccepted() {
        // GIVEN

        // WHEN
        ResponseEntity<Void> response = controller.triggerIndexRebuilding();

        // THEN
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);

        verify(service).triggerDataReindexing();
        verifyNoMoreInteractions(service);
    }

}