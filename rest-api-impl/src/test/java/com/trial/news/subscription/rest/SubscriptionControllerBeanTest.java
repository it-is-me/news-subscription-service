package com.trial.news.subscription.rest;

import com.trial.news.subscription.api.UUIDValidationService;
import com.trial.news.subscription.api.exceptions.UuidValidationException;
import com.trial.news.subscription.domain.api.SubscriptionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionControllerBeanTest {

    private static final String WRONG_UUID = "wrongUuid";
    private static final UUID CORRECT_UUID = UUID.randomUUID();
    private static final String CORRECT_UUID_STRING = CORRECT_UUID.toString();
    private static final Date SUBSCRIBED_SINCE = new Date();

    @Mock
    private SubscriptionService service;

    @Mock
    private UUIDValidationService validator;

    @InjectMocks
    private SubscriptionControllerBean controller;

    @Test
    public void getSubscriptionSuccessSubscribed() {
        // GIVEN
        given(service.getSubscription(UUID.fromString(CORRECT_UUID_STRING)))
                .willReturn(Optional.of(SUBSCRIBED_SINCE));

        // WHEN
        ResponseEntity<Date> response = controller.getSubscription(CORRECT_UUID_STRING);

        // THEN
        assertThat(response).isNotNull();
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        Date body = response.getBody();
        assertThat(body).isEqualTo(SUBSCRIBED_SINCE);

        verify(validator).validateUserUuid(CORRECT_UUID_STRING);
        verifyNoMoreInteractions(validator);

        verify(service).getSubscription(CORRECT_UUID);
        verifyNoMoreInteractions(service);
    }

    @Test
    public void getSubscriptionNotFound() {
        // GIVEN
        given(service.getSubscription(UUID.fromString(CORRECT_UUID_STRING))).willReturn(Optional.empty());

        // WHEN
        ResponseEntity<Date> response = controller.getSubscription(CORRECT_UUID_STRING);

        // THEN
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        Date body = response.getBody();
        assertThat(body).isNull();

        verify(validator).validateUserUuid(CORRECT_UUID_STRING);
        verifyNoMoreInteractions(validator);

        verify(service).getSubscription(CORRECT_UUID);
        verifyNoMoreInteractions(service);
    }

    @Test(expected = UuidValidationException.class)
    public void getSubscriptionValidationError() {
        // GIVEN
        doThrow(new UuidValidationException()).when(validator).validateUserUuid(WRONG_UUID);

        // WHEN
        try {
            controller.getSubscription(WRONG_UUID);
        } catch (UuidValidationException e) {

            // THEN
            verify(validator).validateUserUuid(WRONG_UUID);
            verifyNoMoreInteractions(validator);

            verifyZeroInteractions(service);

            throw e;
        }
    }

    @Test
    public void subscribeCreated() {
        // GIVEN
        given(service.subscribe(CORRECT_UUID)).willReturn(true);

        // WHEN
        ResponseEntity<Void> response = controller.subscribe(CORRECT_UUID_STRING);

        // THEN
        assertThat(response).isNotNull();
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        verify(validator).validateUserUuid(CORRECT_UUID_STRING);
        verifyNoMoreInteractions(validator);

        verify(service).subscribe(CORRECT_UUID);
        verifyNoMoreInteractions(service);
    }

    @Test
    public void subscribeNoContent() {
        // GIVEN
        given(service.subscribe(CORRECT_UUID)).willReturn(false);

        // WHEN
        ResponseEntity<Void> response = controller.subscribe(CORRECT_UUID_STRING);

        // THEN
        assertThat(response).isNotNull();
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        verify(validator).validateUserUuid(CORRECT_UUID_STRING);
        verifyNoMoreInteractions(validator);

        verify(service).subscribe(CORRECT_UUID);
        verifyNoMoreInteractions(service);
    }

    @Test(expected = UuidValidationException.class)
    public void subscribeValidationError() {
        // GIVEN
        doThrow(new UuidValidationException()).when(validator).validateUserUuid(WRONG_UUID);

        // WHEN
        try {
            controller.subscribe(WRONG_UUID);
        } catch (UuidValidationException e) {

            // THEN
            verify(validator).validateUserUuid(WRONG_UUID);
            verifyNoMoreInteractions(validator);

            verifyZeroInteractions(service);

            throw e;
        }
    }

    @Test
    public void unsubscribeSuccess() {
        // GIVEN
        given(service.unsubscribe(CORRECT_UUID)).willReturn(true);

        // WHEN
        ResponseEntity<Void> response = controller.unsubscribe(CORRECT_UUID_STRING);

        // THEN
        assertThat(response).isNotNull();
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

        verify(validator).validateUserUuid(CORRECT_UUID_STRING);
        verifyNoMoreInteractions(validator);

        verify(service).unsubscribe(CORRECT_UUID);
        verifyNoMoreInteractions(service);
    }

    @Test
    public void unsubscribeNotFound() {
        // GIVEN
        given(service.unsubscribe(CORRECT_UUID)).willReturn(false);

        // WHEN
        ResponseEntity<Void> response = controller.unsubscribe(CORRECT_UUID_STRING);

        // THEN
        assertThat(response).isNotNull();
        assertThat(response.getBody()).isNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        verify(validator).validateUserUuid(CORRECT_UUID_STRING);
        verifyNoMoreInteractions(validator);

        verify(service).unsubscribe(CORRECT_UUID);
        verifyNoMoreInteractions(service);
    }

    @Test(expected = UuidValidationException.class)
    public void unsubscribeValidationError() {
        // GIVEN
        doThrow(new UuidValidationException()).when(validator).validateUserUuid(WRONG_UUID);

        // WHEN
        try {
            controller.unsubscribe(WRONG_UUID);
        } catch (UuidValidationException e) {

            // THEN
            verify(validator).validateUserUuid(WRONG_UUID);
            verifyNoMoreInteractions(validator);

            verifyZeroInteractions(service);

            throw e;
        }
    }

}