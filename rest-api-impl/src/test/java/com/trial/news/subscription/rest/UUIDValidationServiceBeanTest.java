package com.trial.news.subscription.rest;

import com.trial.news.subscription.api.exceptions.UuidValidationException;
import org.junit.Test;

public class UUIDValidationServiceBeanTest {

    private static final String CORRECT_UUID = "33c6b326-48c6-4479-9cf1-ba9fe9b70025";
    private static final String EMPTY_UUID = "";
    private static final String INCORRECT_UUID = "wrongUuid";

    private final UUIDValidationServiceBean validator = new UUIDValidationServiceBean();

    @Test
    public void validationSuccess() {
        validator.validateUserUuid(CORRECT_UUID);
    }

    @Test(expected = UuidValidationException.class)
    public void validationExceptionEmpty() {
        validator.validateUserUuid(EMPTY_UUID);
    }

    @Test(expected = UuidValidationException.class)
    public void validationExceptionNull() {
        validator.validateUserUuid(null);
    }

    @Test(expected = UuidValidationException.class)
    public void validationExceptionIncorrectUuid() {
        validator.validateUserUuid(INCORRECT_UUID);
    }


}