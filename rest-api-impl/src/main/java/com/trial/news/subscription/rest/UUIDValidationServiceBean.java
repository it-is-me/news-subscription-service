package com.trial.news.subscription.rest;

import com.trial.news.subscription.api.UUIDValidationService;
import com.trial.news.subscription.api.exceptions.UuidValidationException;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class UUIDValidationServiceBean implements UUIDValidationService {

    private static final String UUID_REGEXP_STRING =
            "([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}";
    private static final Pattern UUID_PATTERN = Pattern.compile(UUID_REGEXP_STRING);

    @Override
    public void validateUserUuid(String userUuid) throws UuidValidationException {
        if (userUuid == null || userUuid.isEmpty()) {
            throw new UuidValidationException("User uuid must not be empty!");
        }
        if (!UUID_PATTERN.matcher(userUuid).matches()) {
            throw new UuidValidationException(String.format("Value %1$s is not a correct UUID!", userUuid));
        }
    }

}
