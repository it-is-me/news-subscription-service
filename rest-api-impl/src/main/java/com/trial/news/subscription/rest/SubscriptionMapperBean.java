package com.trial.news.subscription.rest;

import com.trial.news.subscription.api.dto.SubscriptionDto;
import com.trial.news.subscription.domain.api.Subscription;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.LinkedList;

@Component
public class SubscriptionMapperBean implements SubscriptionMapper {

    public Collection<SubscriptionDto> mapToDto(Collection<Subscription> subscriptions) {
        LinkedList<SubscriptionDto> result = new LinkedList<>();
        for (Subscription subscription : subscriptions) {
            result.addLast(mapToDto(subscription));
        }
        return result;
    }

    private SubscriptionDto mapToDto(Subscription subscription) {
        return new SubscriptionDto(subscription.getUserId(),
                subscription.getSubscribedSince());
    }

}
