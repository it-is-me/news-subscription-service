package com.trial.news.subscription.rest;

import com.trial.news.subscription.api.DistributedIndicesController;
import com.trial.news.subscription.api.Paths;
import com.trial.news.subscription.domain.api.SubscriptionIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DistributedIndicesControllerBean implements DistributedIndicesController {

    private final SubscriptionIndexService indexingService;

    @Autowired
    public DistributedIndicesControllerBean(SubscriptionIndexService indexingService) {
        this.indexingService = indexingService;
    }

    @RequestMapping(
            value = Paths.V1_INDICES,
            method = RequestMethod.PUT
    )
    public ResponseEntity<Void> triggerIndexRebuilding() {
        indexingService.triggerDataReindexing();
        return ResponseEntity.accepted().build();
    }

}
