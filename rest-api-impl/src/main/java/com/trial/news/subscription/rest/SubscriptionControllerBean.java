package com.trial.news.subscription.rest;

import com.trial.news.subscription.api.Paths;
import com.trial.news.subscription.api.SubscriptionController;
import com.trial.news.subscription.api.UUIDValidationService;
import com.trial.news.subscription.domain.api.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@RestController
public class SubscriptionControllerBean implements SubscriptionController {

    private final SubscriptionService service;
    private final UUIDValidationService validator;

    @Autowired
    public SubscriptionControllerBean(SubscriptionService service, UUIDValidationService validator) {
        this.service = service;
        this.validator = validator;
    }

    @RequestMapping(
            value = Paths.V1_USER_SUBSCRIPTION,
            method = RequestMethod.GET
    )
    public ResponseEntity<Date> getSubscription(@PathVariable(name="id") String userUuid){
        validator.validateUserUuid(userUuid);
        Optional<Date> result = service.getSubscription(UUID.fromString(userUuid));
        return result.isPresent() ? ResponseEntity.ok(result.get()) : ResponseEntity.notFound().build();
    }

    @RequestMapping(
            value = Paths.V1_USER_SUBSCRIPTION,
            method = RequestMethod.PUT
    )
    public ResponseEntity<Void> subscribe(@PathVariable(name="id") String userUuid){
        validator.validateUserUuid(userUuid);
        if (service.subscribe(UUID.fromString(userUuid))) {
            return ResponseEntity
                    .created(URI.create(Paths.V1_USER_SUBSCRIPTION.replace("{id}", userUuid))).build();
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @RequestMapping(
            value = Paths.V1_USER_SUBSCRIPTION,
            method = RequestMethod.DELETE
    )
    public ResponseEntity<Void> unsubscribe(@PathVariable(name="id") String userUuid){
        validator.validateUserUuid(userUuid);
        if (service.unsubscribe(UUID.fromString(userUuid))) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
