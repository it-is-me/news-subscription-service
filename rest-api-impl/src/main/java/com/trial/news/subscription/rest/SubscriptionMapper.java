package com.trial.news.subscription.rest;

import com.trial.news.subscription.api.dto.SubscriptionDto;
import com.trial.news.subscription.domain.api.Subscription;

import java.util.Collection;

public interface SubscriptionMapper {

    Collection<SubscriptionDto> mapToDto(Collection<Subscription> subscriptions);


}
