package com.trial.news.subscription.rest;

import com.trial.news.subscription.api.Paths;
import com.trial.news.subscription.api.SubscriptionsController;
import com.trial.news.subscription.api.dto.SubscriptionDto;
import com.trial.news.subscription.domain.api.SubscriptionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Date;

@RestController
public class SubscriptionsControllerBean implements SubscriptionsController {

    private final SubscriptionsService subscriptionService;
    private final SubscriptionMapper subscriptionMapper;

    @Autowired
    public SubscriptionsControllerBean(SubscriptionsService subscriptionService, SubscriptionMapper subscriptionMapper) {
        this.subscriptionService = subscriptionService;
        this.subscriptionMapper = subscriptionMapper;
    }

    @RequestMapping(value = Paths.V1_SUBSCRIPTIONS, method = RequestMethod.GET)
    public ResponseEntity<Collection<SubscriptionDto>> getSubscriptions(
            @RequestParam(name = "from", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date from,
            @RequestParam(name = "to", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date to) {
        if (from == null && to == null) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(subscriptionMapper
                    .mapToDto(subscriptionService
                            .getSubscriptions(from, to)));
        }
    }

}
