package com.trial.newssubscriptionservice;

import com.trial.news.subscription.api.Paths;
import com.trial.news.subscription.api.dto.SubscriptionDto;
import com.trial.news.subscription.domain.statics.IgniteCacheFactoryBean;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.documentationConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes = {
				NewsSubscriptionServiceApplication.class,
				IgniteCacheFactoryBean.class
		},
		webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT
)
@AutoConfigureWebTestClient
@AutoConfigureRestDocs(outputDir = "target/generated-snippets")
public class NewsSubscriptionServiceApplicationIT {

	private static final int SUBSCRIBERS_COUNT_FOR_TEST = 2;
	private static final SimpleDateFormat DATETIME_FORMAT;

	static {
		DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		DATETIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	@Rule
	public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

	private WebTestClient client;

	private Long timeBeforeSubscribed;
	private UUID userIdForSubscriptionGet;
	private UUID userIdForSubscriptionDelete;
	private Long timeAfterSubscribed;

	private Date from;
	private Set<UUID> subscribersHaveToBeReturned;
	private Date to;
	private Set<UUID> subscribersMustNotBeReturned;

	@Before
	public void setUp() {
		client = WebTestClient
				.bindToServer()
				.baseUrl("http://localhost:8080")
				.filter(documentationConfiguration(this.restDocumentation))
				.build();

		userIdForSubscriptionDelete = UUID.randomUUID();
		userIdForSubscriptionGet = UUID.randomUUID();

		timeBeforeSubscribed = System.currentTimeMillis();

		subscribeUser(userIdForSubscriptionDelete);
		subscribeUser(userIdForSubscriptionGet);

		timeAfterSubscribed = System.currentTimeMillis();

		subscribersHaveToBeReturned = new HashSet<>();
		from = new Date();
		for (int i = 0; i < SUBSCRIBERS_COUNT_FOR_TEST; i++) {
			UUID newSubscriber = UUID.randomUUID();
			subscribersHaveToBeReturned.add(newSubscriber);
			subscribeUser(newSubscriber);
		}
		to = new Date();

		subscribersMustNotBeReturned = new HashSet<>();
		for (int i = 0; i < SUBSCRIBERS_COUNT_FOR_TEST; i++) {
			UUID newSubscriber = UUID.randomUUID();
			subscribersMustNotBeReturned.add(newSubscriber);
			subscribeUser(newSubscriber);
		}
	}

	@Test
	public void putSuccess() {
		UUID userId = UUID.randomUUID();
		client.put().uri(Paths.V1_USER_SUBSCRIPTION, userId)
				.exchange()
				.expectStatus().isCreated()
				.expectBody(Void.class)
				.consumeWith(document("put"));
	}

	@Test
	public void getSuccess() {
		Date responseBody =
				client.get().uri(Paths.V1_USER_SUBSCRIPTION, userIdForSubscriptionGet)
						.exchange()
						.expectStatus().isOk()
						.expectBody(Date.class)
						.consumeWith(document("get"))
						.returnResult().getResponseBody();

		assertThat(responseBody).isNotNull();

		assertThat(responseBody.getTime()).isBetween(timeBeforeSubscribed, timeAfterSubscribed);
	}

	@Test
	public void deleteSuccess() {
		client.delete().uri(Paths.V1_USER_SUBSCRIPTION, userIdForSubscriptionDelete)
				.exchange()
				.expectStatus().isNoContent()
				.expectBody(Void.class)
				.consumeWith(document("delete"));
	}

	@Test
	public void getUnknownUser() {
		UUID userId = UUID.randomUUID();

		client.get().uri(Paths.V1_USER_SUBSCRIPTION, userId).exchange()
				.expectStatus().isNotFound()
				.expectBody(Void.class)
				.consumeWith(document("getNotFound"));
	}

	@Test
	public void getAllSubscribedInInterval() {
		List<SubscriptionDto> body = client.get().uri(uriBuilder -> uriBuilder.path(Paths.V1_SUBSCRIPTIONS)
				.queryParam("from", convertDateToIso8601DateTimeString(from))
				.queryParam("to", convertDateToIso8601DateTimeString(to)).build())
				.exchange()
				.expectStatus().isOk()
				.expectBodyList(SubscriptionDto.class)
				.consumeWith(document("getSubscribers"))
				.hasSize(SUBSCRIBERS_COUNT_FOR_TEST)
				.returnResult()
				.getResponseBody();

		assertThat(body).isNotNull();

		body.forEach(subscriber -> {
			assertThat(subscribersMustNotBeReturned.contains(subscriber.getUserId())).isFalse();
			assertThat(subscribersHaveToBeReturned.contains(subscriber.getUserId())).isTrue();
			assertThat(subscriber.getSubscribedSince()).isBetween(from, to);

		});
	}

	private void subscribeUser(UUID userId) {
		client.put().uri(Paths.V1_USER_SUBSCRIPTION, userId)
				.exchange()
				.expectStatus().isCreated();
	}

	private String convertDateToIso8601DateTimeString(Date date) {
		return DATETIME_FORMAT.format(date);
	}

}
