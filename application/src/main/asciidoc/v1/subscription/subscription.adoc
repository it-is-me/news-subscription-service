
The Subscription Service allows to subscribe customer to or unsubscribe from the
newsletter subscription.

==== Subscribe customer to the newsletter

include::operations/put.adoc[]

==== Unsubscribe customer from the newsletter

include::operations/delete.adoc[]

==== Get customer's subscription state

include::operations/get.adoc[]