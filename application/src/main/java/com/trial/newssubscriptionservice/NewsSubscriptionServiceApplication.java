package com.trial.newssubscriptionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.trial")
public class NewsSubscriptionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsSubscriptionServiceApplication.class, args);
	}

}
