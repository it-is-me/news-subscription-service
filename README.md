# Newsletter subscription service

## Overview

The service provides functionality to subscribe, unsubscribe, check, get subscribed users.

The service is designed as a distributed scalable system optimised for READ-performance

## How to build and run

### Pre-requisite

Mandatory:  
Java-8 (strict requirement, because current version of the Ignite uses native off-heap storage and designed for this Java version)  
Maven (not to old, tested for 3.5.2)  
Port 8080 open  
extra-rights can be requested for windows (clustering uses bidirectional connections inside port range 47000 - 49000)

Optional:  
Docker installed and being run  
Docker internal network to be configured (configured by default, needed for service automatic tcp discovery)  
Broadband internet connection (to load base docker image)  
Insomnia for manual testing and firing requests  

### Building and running instructions

To build the entire build including docker container:  
mvn install  

To build limited build with asciidoc and integration tests without docker image:  
mvn validate  

To run docker cluster containing two nodes:  
mvn install  
docker run -d -p 20080:8080 trial/application  
docker run -d -p 21080:8080 trial/application  

To check service work manually  
After deploying the docker container (and waiting some time until application is started) use
Insomnia to fire the requests (import Insomnia_2018-09-22.json request collection)


## Architectural concept

The service is built upon distributed in-memory storage (Apache.Ignite technology is used)
NOTE: Permanent persistence layer can be easily enabled, but will slow down the application and require
small tweaks on data extraction from HD (indices processing)

There are two underlying caches:   
Data cache  
Index cache  

Data cache is implemented as a REPLICATED Ignite cache,  
Index cache is implemented as a set of local caches.  

Data cache structure is more-like distributed B+ tree of HashMaps, controlled
by Ignte and synchronised between all application instances, read optimised (small B+ level, big HashMap level)

Index cache is implemented as a ConcurrentSkipListMap (tree-like structure, keeping sorted key sets). 
Index collision resolution mechanism is also in place.

Optimised operations:
Get subscription, Get subscription list in interval (from/to)

Limitations:
WRITE operations (put/delete). They require necessity of the data synchronisation between the cluster nodes 

## Clustering and scalability

The service should be run with a load balancer (out of scope, networking)

1. Run more docker containers
2. Wait until the customer data will be automatically migrated to the new node (data cache is replicated)
3. Trigger cluster index rebuilding on any node once
4. Configure load balancer to forward requests to the new node (out of scope)

Last step can be easily automated by Ignite start hook, but was not automated intentionally because index rebuilding operation
is quite expensive and, more preferable, should be run in the low-loads period of time (e.g. early morning / night). 

## Automatic documentation

Asciidocs are in place, check application/target/generated-docs/index.html after building

## Monitoring 

Spring.Actuator Health-check, statistics are in place. Monitoring can be easily extended in case of need.

## Possible improvements:

Automatic cloud deployment including load balancer  
Monitoring to be improved  
Adding node cluster automation  
Automatic load testing, integration with JMeter test scenario  
