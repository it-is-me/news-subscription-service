package com.trial.news.subscription.api.dto;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class SubscriptionDtoTest {

    private static final UUID USER_ID = UUID.randomUUID();
    private static final Date SUBSCRIBED_SINCE = new Date();

    private SubscriptionDto subscriptionDto;

    @Before
    public void setUp() {
        subscriptionDto = new SubscriptionDto(USER_ID, SUBSCRIBED_SINCE);
    }

    @Test
    public void getUserId() {
        assertThat(subscriptionDto.getUserId()).isEqualTo(USER_ID);
    }

    @Test
    public void isSubscribed() {
        assertThat(subscriptionDto.getSubscribedSince()).isEqualTo(SUBSCRIBED_SINCE);
    }

}