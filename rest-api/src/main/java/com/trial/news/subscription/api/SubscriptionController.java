package com.trial.news.subscription.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
public interface SubscriptionController {

    /**
     * Checks if the given user subscribed to the newsletter or not
     *
     * @param userUuid the user's uuid
     * @return {@link org.springframework.http.HttpStatus#OK} if user is subscribed and the
     *      subscription date or {@link org.springframework.http.HttpStatus#NOT_FOUND} if the
     *      user with the given uuid is not subscribed (subscription does not exist)
     */
    @RequestMapping(
            value = Paths.V1_USER_SUBSCRIPTION,
            method = RequestMethod.GET
    )
    ResponseEntity<Date> getSubscription(@PathVariable(name="id") String userUuid);

    /**
     * Subscribes the given user to the newsletter
     *
     * @param userUuid the user's uuid
     * @return {@link org.springframework.http.HttpStatus#CREATED} if the user was subscribed
     *      by the result of the current request
     *      or {@link org.springframework.http.HttpStatus#NO_CONTENT} if the
     *      user with the given uuid was subscribed before
     */
    @RequestMapping(
            value = Paths.V1_USER_SUBSCRIPTION,
            method = RequestMethod.PUT
    )
    ResponseEntity<Void> subscribe(@PathVariable(name="id") String userUuid);


    /**
     * Unsubscribes the given user from the newsletter subscription
     *
     * @param userUuid the user's uuid
     * @return {@link org.springframework.http.HttpStatus#NO_CONTENT} if user was unsubscribed
     *      or {@link org.springframework.http.HttpStatus#NOT_FOUND} if the
     *      user with the given uuid is not subscribed (subscription does not exist)
     */
    @RequestMapping(
            value = Paths.V1_USER_SUBSCRIPTION,
            method = RequestMethod.DELETE
    )
    ResponseEntity<Void> unsubscribe(@PathVariable(name="id") String userUuid);

}
