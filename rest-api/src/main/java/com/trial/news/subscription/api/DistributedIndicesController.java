package com.trial.news.subscription.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface DistributedIndicesController {

    @RequestMapping(
            value = Paths.V1_INDICES,
            method = RequestMethod.PUT
    )
    ResponseEntity<Void> triggerIndexRebuilding();

}
