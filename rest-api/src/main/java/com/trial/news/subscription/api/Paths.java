package com.trial.news.subscription.api;

public interface Paths {
    String V1 = "/v1";
    String USERS = "/users";
    String V1_USERS = V1 + USERS;
    String USER = "/{id}";
    String V1_USER = V1_USERS + USER;
    String SUBSCRIPTION = "/subscription";
    String V1_USER_SUBSCRIPTION = V1_USER + SUBSCRIPTION;
    String SUBSCRIPTIONS = "/subscriptions";
    String V1_SUBSCRIPTIONS = V1 + SUBSCRIPTIONS;
    String INDICES = "/indices";
    String V1_INDICES = V1 + INDICES;
}
