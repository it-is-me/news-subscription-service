package com.trial.news.subscription.api;

import com.trial.news.subscription.api.exceptions.UuidValidationException;

import java.util.UUID;

/**
 * The service provides functionality of UUID validation.
 * FYI: Java {@link UUID} has broken internal validation, that is the reason why
 * it is not used in the controller directly. There is also another way to make
 * this validation work using spring paths validator, but implementing it requires
 * more efforts
 */
public interface UUIDValidationService {

    void validateUserUuid(String userUuid) throws UuidValidationException;

}
