package com.trial.news.subscription.api;

import com.trial.news.subscription.api.dto.SubscriptionDto;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Date;

@RestController
public interface SubscriptionsController {

    @RequestMapping(
            value = Paths.V1_SUBSCRIPTIONS,
            method = RequestMethod.GET
    )
    ResponseEntity<Collection<SubscriptionDto>> getSubscriptions(
            @RequestParam(name = "from", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date from,
            @RequestParam(name = "to", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date to);

}
