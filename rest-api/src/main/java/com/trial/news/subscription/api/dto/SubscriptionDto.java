package com.trial.news.subscription.api.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class SubscriptionDto implements Serializable {

    private final UUID userId;
    private final Date subscribedSince;

    @JsonCreator
    public SubscriptionDto(@JsonProperty("userId") UUID userId,
                           @JsonProperty("subscribedSince") Date subscribedSince) {
        this.userId = userId;
        this.subscribedSince = subscribedSince;
    }

    public UUID getUserId() {
        return userId;
    }

    public Date getSubscribedSince() {
        return subscribedSince;
    }

}
