package com.trial.news.subscription.api.exceptions;

public class UuidValidationException extends RuntimeException {

    public UuidValidationException() {
        super();
    }

    public UuidValidationException(String message) {
        super(message);
    }

    public UuidValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
