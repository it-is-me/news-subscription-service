package com.trial.news.subscription.domain.api;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class Subscription {

    private final UUID userId;
    private final Date subscribedSince;

    public Subscription(UUID userId, Date subscribedSince) {
        this.userId = userId;
        this.subscribedSince = subscribedSince;
    }

    public UUID getUserId() {
        return userId;
    }

    public Date getSubscribedSince() {
        return subscribedSince;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscription that = (Subscription) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(subscribedSince, that.subscribedSince);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, subscribedSince);
    }
}
