package com.trial.news.subscription.domain.api;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;

import java.util.UUID;

public interface IgniteCacheFactory {

    /**
     * Provides the Ignite instance
     * @return the instance
     */
    Ignite getIgniteInstance();

    /**
     * Constructs distributed cache
     *
     * @return the cache
     */
    IgniteCache<UUID, Long> getSubscriptionCache();
}
