package com.trial.news.subscription.domain;

import com.trial.news.subscription.domain.api.SubscriptionIndexService;
import com.trial.news.subscription.domain.api.SubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionIndexServiceBean implements SubscriptionIndexService {

    private final SubscriptionRepository repository;

    @Autowired
    public SubscriptionIndexServiceBean(SubscriptionRepository repository) {
        this.repository = repository;
    }

    @Override
    public void triggerDataReindexing() {
        repository.triggerGlobalReindexing();
    }

}
