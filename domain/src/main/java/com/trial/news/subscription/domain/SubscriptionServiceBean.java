package com.trial.news.subscription.domain;

import com.trial.news.subscription.domain.api.SubscriptionRepository;
import com.trial.news.subscription.domain.api.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class SubscriptionServiceBean implements SubscriptionService {

    private final SubscriptionRepository repository;

    @Autowired
    public SubscriptionServiceBean(SubscriptionRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Date> getSubscription(UUID userId) {
        return repository.getSubscription(userId);
    }

    @Override
    public boolean subscribe(UUID userId) {
        return repository.putSubscription(userId);
    }

    @Override
    public boolean unsubscribe(UUID userId) {
        return repository.deleteSubscription(userId);
    }

}
