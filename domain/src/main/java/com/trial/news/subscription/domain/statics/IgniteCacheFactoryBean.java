package com.trial.news.subscription.domain.statics;

import com.trial.news.subscription.domain.api.IgniteCacheFactory;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.CacheAtomicityMode;
import org.apache.ignite.cache.CacheMode;
import org.apache.ignite.configuration.CacheConfiguration;

import java.util.UUID;

public class IgniteCacheFactoryBean implements IgniteCacheFactory {

    private static final IgniteCacheFactoryBean INSTANCE = new IgniteCacheFactoryBean();

    private final IgniteCache<UUID, Long> subscriptionCache;
    private final Ignite ignite;

    private IgniteCacheFactoryBean() {
        ignite = Ignition.start();
        subscriptionCache = configureCache();
    }

    /*
        Constructor only for tests
     */
    IgniteCacheFactoryBean(Ignite ignite) {
        this.ignite = ignite;
        subscriptionCache = configureCache();
    }

    IgniteCache<UUID, Long> configureCache() {
        return ignite.getOrCreateCache(createCacheConfiguration());
    }

    CacheConfiguration<UUID, Long> createCacheConfiguration() {
        CacheConfiguration<UUID, Long> cacheConfiguration = new CacheConfiguration<>("Subscriptions");
        cacheConfiguration.setCacheMode(CacheMode.REPLICATED);
        cacheConfiguration.setAtomicityMode(CacheAtomicityMode.ATOMIC);
        return cacheConfiguration;
    }

    @Override
    public IgniteCache<UUID, Long> getSubscriptionCache() {
        return subscriptionCache;
    }

    @Override
    public Ignite getIgniteInstance() {
        return ignite;
    }

    public static IgniteCacheFactoryBean instance() {
        return INSTANCE;
    }

}
