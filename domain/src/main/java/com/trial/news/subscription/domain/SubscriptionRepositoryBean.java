package com.trial.news.subscription.domain;

import com.trial.news.subscription.domain.api.Subscription;
import com.trial.news.subscription.domain.api.SubscriptionRepository;
import com.trial.news.subscription.domain.statics.IgniteCacheFactoryBean;
import com.trial.news.subscription.domain.statics.LocalBinaryTreeIndex;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.lang.IgniteRunnable;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.Optional;
import java.util.UUID;

@Component
public class SubscriptionRepositoryBean implements SubscriptionRepository {

    private final Ignite ignite;
    private final IgniteCache<UUID, Long> subscriptionsCache;

    public SubscriptionRepositoryBean() {
        ignite = IgniteCacheFactoryBean.instance().getIgniteInstance();
        subscriptionsCache = IgniteCacheFactoryBean.instance().getSubscriptionCache();
    }

    public SubscriptionRepositoryBean(Ignite ignite,
                                      IgniteCache<UUID, Long> subscriptionsCache) {
        this.ignite = ignite;
        this.subscriptionsCache = subscriptionsCache;
    }

    @Override
    public Optional<Date> getSubscription(UUID userId) {
        Long timeInMillis = subscriptionsCache.get(userId);
        return timeInMillis == null ? Optional.empty() : Optional.of(new Date(timeInMillis));
    }

    @Override
    public boolean putSubscription(UUID userId) {
        Long subscribedSince = System.currentTimeMillis();
        if (subscriptionsCache.putIfAbsent(userId, subscribedSince)) {
            broadcastIndexCreate(subscribedSince, userId);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteSubscription(UUID userId) {
        Long subscribedSince = subscriptionsCache.get(userId);
        if (subscribedSince == null) return false;

        subscriptionsCache.remove(userId);
        broadcastIndexRemove(subscribedSince, userId);
        return true;
    }

    @Override
    public Collection<Subscription> getSubscribedBetween(Date after, Date before) {
        Collection<UUID> subscriptionKeys =
                LocalBinaryTreeIndex.instance().findSubscriptionKeys(after.getTime(), before.getTime());

        LinkedList<Subscription> result = new LinkedList<>();

        for (UUID uuid : subscriptionKeys) {
            Long subscribedSinceInMillis = subscriptionsCache.get(uuid);
            if (subscribedSinceInMillis != null) {
                result.add(new Subscription(uuid, new Date(subscribedSinceInMillis)));
            }
        }

        return result;
    }

    @Override
    public void triggerGlobalReindexing() {
        broadcastFullIndexRebuilding();
    }

    private void broadcastIndexRemove(Long subscribedSinceInMillis, UUID userId) {
        ignite.compute().broadcast((IgniteRunnable) () ->
                LocalBinaryTreeIndex.instance().removeIndexForEntry(subscribedSinceInMillis, userId));
    }

    private void broadcastIndexCreate(Long subscribedSinceInMillis, UUID userId) {
        ignite.compute().broadcast((IgniteRunnable) () ->
                LocalBinaryTreeIndex.instance().indexEntry(subscribedSinceInMillis, userId, false));
    }

    private void broadcastFullIndexRebuilding() {
        ignite.compute().runAsync((IgniteRunnable) () -> {

            IgniteCache<UUID, Long> subscriptionCache = IgniteCacheFactoryBean.instance().getSubscriptionCache();
            LocalBinaryTreeIndex indexer = LocalBinaryTreeIndex.instance();

            subscriptionCache
                    .localEntries()
                    .forEach(e -> indexer.indexEntry(e.getValue(), e.getKey(), true));

        });
    }

}
