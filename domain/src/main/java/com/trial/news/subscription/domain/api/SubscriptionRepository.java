package com.trial.news.subscription.domain.api;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public interface SubscriptionRepository {

    Optional<Date> getSubscription(UUID userId);

    boolean putSubscription(UUID userId);

    boolean deleteSubscription(UUID userId);

    Collection<Subscription> getSubscribedBetween(Date after, Date before);

    void triggerGlobalReindexing();

}
