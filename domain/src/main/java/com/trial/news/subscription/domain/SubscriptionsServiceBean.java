package com.trial.news.subscription.domain;

import com.trial.news.subscription.domain.api.Subscription;
import com.trial.news.subscription.domain.api.SubscriptionRepository;
import com.trial.news.subscription.domain.api.SubscriptionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;

@Component
public class SubscriptionsServiceBean implements SubscriptionsService {

    private final SubscriptionRepository repository;

    @Autowired
    public SubscriptionsServiceBean(SubscriptionRepository repository) {
        this.repository = repository;
    }

    @Override
    public Collection<Subscription> getSubscriptions(Date after, Date before) {
        return repository.getSubscribedBetween(after, before);
    }

}
