package com.trial.news.subscription.domain.statics;

import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListMap;

public final class LocalBinaryTreeIndex {

    private static final LocalBinaryTreeIndex INSTANCE = new LocalBinaryTreeIndex();

    private final ConcurrentSkipListMap<Long, ConcurrentLinkedQueue<UUID>> indexStructure;

    private LocalBinaryTreeIndex() {
        indexStructure = new ConcurrentSkipListMap<>();
    }

    /**
     * WARNING!
     * This method must not be called twice for the same userId with a different time
     *
     * userId collisions must be resolved externally (via cache.containsKey(userId) and etc.)
     *
     * @param timeInMillis subscribedSince (exactly the same time as in the cache)
     * @param userId user's uuid
     * @param reindexing reindexing flag, used for optimizations
     */
    public void indexEntry(Long timeInMillis, UUID userId, boolean reindexing) {
        ConcurrentLinkedQueue<UUID> collisionsResolver = indexStructure.get(timeInMillis);

        if (collisionsResolver == null) {
            collisionsResolver = new ConcurrentLinkedQueue<>();
            ConcurrentLinkedQueue<UUID> tmp = indexStructure.putIfAbsent(timeInMillis, collisionsResolver);
            if (tmp != null) {
                collisionsResolver = tmp;
            }
        }
        // to avoid duplications and allow system to work during reindexing
        if (reindexing) collisionsResolver.remove(userId);
        collisionsResolver.add(userId);
    }

    public void removeIndexForEntry(Long timeInMillis, UUID userId) {
        ConcurrentLinkedQueue<UUID> collisionsResolver = indexStructure.get(timeInMillis);
        if (collisionsResolver == null) return;
        collisionsResolver.remove(userId);
    }

    public Collection<UUID> findSubscriptionKeys(Long afterTimeInMillis, Long beforeTimeInMillis) {
        LinkedList<UUID> keys = new LinkedList<>();

        indexStructure.subMap(afterTimeInMillis, false, beforeTimeInMillis, false)
                .forEach((k, v) -> keys.addAll(v));

        return keys;
    }

    public static LocalBinaryTreeIndex instance() {
        return INSTANCE;
    }
}
