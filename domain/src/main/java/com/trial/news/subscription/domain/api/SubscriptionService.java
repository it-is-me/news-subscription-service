package com.trial.news.subscription.domain.api;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

/**
 * The service is responsible for the subscription management
 */
public interface SubscriptionService {

    Optional<Date> getSubscription(UUID userId);

    boolean subscribe(UUID userId);

    boolean unsubscribe(UUID userId);

}
