package com.trial.news.subscription.domain.api;

import java.util.Collection;
import java.util.Date;

public interface SubscriptionsService {

    Collection<Subscription> getSubscriptions(Date after, Date before);
}
