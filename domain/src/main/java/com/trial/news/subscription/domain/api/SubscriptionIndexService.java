package com.trial.news.subscription.domain.api;

public interface SubscriptionIndexService {

    void triggerDataReindexing();
}
