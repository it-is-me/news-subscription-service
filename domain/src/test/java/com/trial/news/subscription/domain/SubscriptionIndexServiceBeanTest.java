package com.trial.news.subscription.domain;

import com.trial.news.subscription.domain.api.SubscriptionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionIndexServiceBeanTest {

    @Mock
    private SubscriptionRepository repository;

    @InjectMocks
    private SubscriptionIndexServiceBean service;

    @Test
    public void triggerDataReindexing() {
        // GIVEN

        // WHEN
        service.triggerDataReindexing();

        // THEN
        verify(repository).triggerGlobalReindexing();
        verifyNoMoreInteractions(repository);
    }
}