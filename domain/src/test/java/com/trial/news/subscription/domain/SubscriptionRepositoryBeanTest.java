package com.trial.news.subscription.domain;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionRepositoryBeanTest {

    private static final UUID USER_ID = UUID.randomUUID();
    private static final Long SUBSCRIBED_SINCE = 777L;

    @Mock
    private Ignite ignite;

    @Mock
    private IgniteCache<UUID, Long> igniteCache;

    private SubscriptionRepositoryBean repository;

    @Before
    public void setUp() {
        repository = new SubscriptionRepositoryBean(ignite, igniteCache);
    }

    @Test
    public void getSubscription() {
        // GIVEN
        given(igniteCache.get(USER_ID)).willReturn(SUBSCRIBED_SINCE);

        // WHEN
        Optional<Date> result = repository.getSubscription(USER_ID);

        // THEN
        assertThat(result).isNotNull();
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(new Date(SUBSCRIBED_SINCE));

        verify(igniteCache).get(USER_ID);
        verifyNoMoreInteractions(igniteCache);

        verifyZeroInteractions(ignite);
    }

    @Test
    public void getSubscriptionKeyNotFound() {
        // GIVEN
        given(igniteCache.get(USER_ID)).willReturn(null);

        // WHEN
        Optional<Date> result = repository.getSubscription(USER_ID);

        // THEN
        assertThat(result).isNotNull();
        assertThat(result.isPresent()).isFalse();

        verify(igniteCache).get(USER_ID);
        verifyNoMoreInteractions(igniteCache);

        verifyZeroInteractions(ignite);
    }

    // Other methods are covered by the integration tests

}