package com.trial.news.subscription.domain;

import com.trial.news.subscription.domain.api.SubscriptionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionServiceBeanTest {

    private static final UUID USER_UUID = UUID.randomUUID();
    private static final Date SUBSCRIBED_SINCE = new Date();

    @Mock
    private SubscriptionRepository repository;

    @InjectMocks
    private SubscriptionServiceBean service;

    @Test
    public void getSubscription() {
        // GIVEN
        given(repository.getSubscription(USER_UUID)).willReturn(Optional.of(SUBSCRIBED_SINCE));

        // WHEN
        Optional<Date> result = service.getSubscription(USER_UUID);

        // THEN
        assertThat(result).isNotNull();
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(SUBSCRIBED_SINCE);

        verify(repository).getSubscription(USER_UUID);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void subscribe() {
        // GIVEN

        // WHEN
        service.subscribe(USER_UUID);

        // THEN
        verify(repository).putSubscription(USER_UUID);
        verifyNoMoreInteractions(repository);
    }

    @Test
    public void unsubscribe() {
        // GIVEN

        // WHEN
        service.unsubscribe(USER_UUID);

        // THEN
        verify(repository).deleteSubscription(USER_UUID);
        verifyNoMoreInteractions(repository);
    }

}